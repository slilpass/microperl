# README #

Скрипт `mm-cron.pl` суется в кронтаб, шарашит раз в сутки 30 сек, создает файл `mm.csv`:

`crontab -l`

`0 3 * * * mm-cron.pl`

Скрипт `mm` запускается через `perl mm daemon` и показывает файл `mm.csv`