#!/usr/bin/perl

# crontab -e
# 0 3 * * * mm-cron.pl

use 5.010;
use Mojo::UserAgent;
use Mojo::DOM;

open MM, '>mm.csv';
$time = time;

my $ua = Mojo::UserAgent->new;
my $t = $ua->transactor;
$ua = $ua->transactor(Mojo::UserAgent::Transactor->new);
$ua->transactor->name('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36');

my $a50 = $ua->get('https://www.similarweb.com/top-websites/russian-federation/')
    ->result->dom
    ->find('a.topRankingGrid-blankLink')
    ->map(attr => 'href');

my %yandex, %google, %len;
my $cnt = 1;

for $site (@$a50) {
    my $res = $ua->get($site)->result;
    print MM "\n", $cnt++, "\t", $site, "\t", $res->code, "\t";
    while (
        $res->code == 301    # all (42)
        or $res->code == 302 # (+)yandex.ru (-)(-)google.com/ru (-)facebook.com
        or $res->code == 303 # kp.ru
        or $res->code == 308 # pinterest.ru
                             # 403 mvideo.ru
    ) {
        my $url = $res->headers->location;
        print MM ',', $res->code, '>', $url;
        $url = substr($url, 0, 1) eq '/' ? $site . $url : $url;
        $res = $ua->get($url)->result;
    }
    $len{$site} = length $res->body;
    print MM "\t", $len{$site}, "\t", $len{$site} < 3000 ? '<3' : '';
    my @words = ($res->body =~ /(mc.yandex.ru|googletagmanager.com|google-analytics.com)/ig);
    for (@words) {
        if ($_ eq 'mc.yandex.ru') {
            $yandex{$site}++
        }else{
            $google{$site}++
        }
    }
    print MM "\t", $yandex{$site} > 0 ? 'Y' : '', "\t", $google{$site} ? 'G' : '';
}

close MM;
say time - $time;
