#!/usr/bin/env perl
use Mojolicious::Lite -signatures;

get '/' => sub ($c) {
  my $sites;
  open MM, '<mm.csv';
  while (<MM>) {
    my @line = split /\t/;
    $sites->{$line[0]} = \@line if @line > 1;
  }
  close MM;
  $c->render(
      template => 'index',
      format   => 'html',
      sites    => $sites,
  );};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
__DATA__

@@ index.html.ep
% layout 'default';

<table border="1" cellpadding="5" cellspacing="0">
<thead><tr><th>#</th><th>Сайт</th><th>длина</th><th>Яндекс.Метрика</th><th>Google.Аналитика</th> </tr></thead>
<tbody>
% foreach (sort { $a <=> $b } keys %$sites) {
<tr <%= $sites->{$_}->[5] ? 'style=background-color:#F88;' : '' %>>
    <td align="center"><%= $_ %></td>
    <td align="center"><a href=<%= $sites->{$_}->[1] %> title="<%= $sites->{$_}->[3] %>"><%= $sites->{$_}->[1] %></a></td>
    <td align="center"><%= $sites->{$_}->[4] %></td>
    <td align="center"><%= $sites->{$_}->[6] %></td>
    <td align="center"><%= $sites->{$_}->[7] %></td>
</tr>
% }
</tbody>
</table>

@@ layouts/default.html.ep
<!doctype html><html>
  <body><%= content %></body>
</html>